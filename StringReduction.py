__author__ = 'alexf4'

import sys

def main():

    #Grab the input data
    T =  sys.stdin.readline()

    strings = []

    #Clean input
    for repeater in range (0,int(T)):
        inputdata = sys.stdin.readline()
        strings.append(inputdata.rstrip())

    #for each element in strings
    for string in strings:

        score = 0

        #generate succicies
        score = scorefinder(string)


        print score

def generateSufficies(input1):
    retval = list()

    if (len(input1)  == 1):
        databack = [input1]
        return databack
    else :
        retval.append(input1)
        temp = (generateSufficies(input1[1:]))

        retval.extend(temp)
    return retval


def scorefinder(input):
    data = list(input)

    retval = evaluateString(data)


    return len(retval)


def evaluateString(input):

    retString = ""



    if checkAllSameChars(input):

        return input


    else:

        matches = findMatches(input)

        tempString = ""

        for x in matches:
            tempString = modInPlace(input, x)

        return evaluateString(tempString)






def modInPlace(input, x):

    front = input[:x]
    mid = reduceString(input[x:x+1])
    end = input[x+1:]

    retString = front + mid + end

    return retString

def findMatches(input):
    retArray = []

    previous = True

    x = 0

    while x < (len(input)):
        if input[x] != input[x+1] and (previous):
            retArray.append(x)
            previous = False
            x = x +2
        else:
            previous = True
            x = x + 1



def checkAllSameChars(input):
    testChar = input[0]

    teststring = string.join(input)


    if testChar == "a":
        index1 = teststring.find("b")
        index2 = teststring.find("c")
        if (index1 != -1 and index2 != -1):
            return True

    elif testChar == "b":
        index1 = teststring.find("a")
        index2 = teststring.find("c")
        if (index1 != -1 or index2 != -1):
            return True

    elif testChar == "c":
        index1 = teststring.find("b")
        index2 = teststring.find("a")
        if (index1 != -1 or index2 != -1):
            return True

    return False

def reduceString(input):

    stringValue = string.join(input)

    if stringValue == "ab" or stringValue == "ba":
        return "c"
    elif stringValue == "ac" or stringValue == ac:
        return "b"
    elif stringValue == "bc" or stringValue == "cb":
        return "a"

if __name__ == "__main__":
    main()

